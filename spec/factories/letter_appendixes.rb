# frozen_string_literal: true

FactoryBot.define do
  factory :letter_appendix do
    letter nil
    name 'MyString'
  end
end
